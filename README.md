
NOTE: initially created from:  cordova-plugin-datecs-printer

The first thing that you must know is that the plugin is available through this variable `window.iPosPrinter`.

*As well as any other plugin it will only be available after `deviceready` event is fired*

So, there's a lot of functions that you can call to execute each operation and perform the printer actions, these are the most important ones (you can see all on [ipos-printer.js](www/ipos-printer.js) file):

_(every function accept at least two parameters, and they're the last ones: onSuccess function and onError function)_

### Example
```javascript

//basic plugin test: 
window.iPosPrinter.greet("World", greetSuccess, iPosPrinterFailure);

//loadPrinter:
window.iPosPrinter.loadPrinter( null, iPosPrinterFailure);

// print receipt:
// uses these vars: trx_type, buyer_co_name, seller_co_name, amount, approval_num, onSuccessFunction, onErrorFunction
window.iPosPrinter.printReceipt("Sale", "Buyer Test Co.", "Seller Test Co.", "1.99", "0000000",null, iPosPrinterFailure);

function greetSuccess(message) {
    alert(message);
}

function iPosPrinterFailure(message='') {
    if (message !== '') {
        alert('Error:'+message);
    } else {
        alert("Error");
    } 
}


## ConnectionStatus Event

To listen about the connection status this is the way you should go:
You should use this plugin to receive the broadcasts `cordova plugin add cordova-plugin-broadcaster`

```javascript
window.broadcaster.addEventListener( "DatecsPrinter.connectionStatus", function(e) {
  if (e.isConnected) {
    //do something
  }
});
```

## Angular / Ionic

If your intention is to use it with Angular or Ionic, you may take a look at this simple example: https://github.com/giorgiofellipe/cordova-plugin-datecsprinter-example.
There's a ready to use angular service implementation.
