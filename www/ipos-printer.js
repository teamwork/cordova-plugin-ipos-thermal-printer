var exec = require('cordova/exec');

var printer = {
  platforms: ['android'],

  isSupported: function() {
    if (window.device) {
      var platform = window.device.platform;
      if ((platform !== undefined) && (platform !== null)) {
        return (this.platforms.indexOf(platform.toLowerCase()) >= 0);
      }
    }
    return false;
  },

  loadPrinter: function( onSuccess, onError) {
    exec(onSuccess, onError, 'iPosPrinter', 'loadPrinter', []);
  },

  //trx_type, buyer_co_name, seller_co_name, amount, approval_num
  printReceipt: function( trx_type, buyer_co_name, seller_co_name, amount, approval_num, onSuccess, onError) {
    exec(onSuccess, onError, 'iPosPrinter', 'printReceipt', [trx_type, buyer_co_name, seller_co_name, amount, approval_num]);
  },

  
  bluetoothPrinterTest: function( onSuccess, onError) {
    exec(onSuccess, onError, 'iPosPrinter', 'bluetoothPrinterTest', []);
  },

  printBitmapTest: function( onSuccess, onError) {
    exec(onSuccess, onError, 'iPosPrinter', 'printBitmapTest', []);
  },
  

  greet: function(name, onSuccess, onError) {
    exec(onSuccess, onError, 'iPosPrinter', 'greet', [name]);
  },

  toast: function( onSuccess, onError) {
    exec(onSuccess, onError, 'iPosPrinter', 'toast', []);
  },

  testNoAction: function( onSuccess, onError) {
    exec(onSuccess, onError, 'iPosPrinter', 'test', []);
  }

};
module.exports = printer;