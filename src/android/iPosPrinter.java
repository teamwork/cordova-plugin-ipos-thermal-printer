package com.teamwork.iposprinter;
// The native Toast API
import android.widget.Toast;
// cordova libs
import org.apache.cordova.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import java.net.URL;
import java.io.ByteArrayOutputStream;

//bluetooth objects
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

//import custom bluetooth utils
import com.android.bluetoothprinter.Utils.BluetoothUtil;
import com.android.bluetoothprinter.Utils.ESCUtil;
import com.android.bluetoothprinter.Utils.BitMapUtil;

import com.android.bluetoothprinter.ThreadPoolManager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.content.res.Resources;

public class iPosPrinter extends CordovaPlugin {
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothDevice mBluetoothPrinterDevice = null;
    private BluetoothSocket socket = null;

    private boolean isBluetoothOpen = false;

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {
        
        String message = null;
        
        switch (action) {
            case "greet":
                String name = data.getString(0);
                message = "Hello mah " + name;
                callbackContext.success(message);
                break;
            case "toast":
                // Create the toast
                message = "Toast Success";
                Toast toast = Toast.makeText(cordova.getActivity(), message, Toast.LENGTH_LONG );
                // Display toast
                toast.show();
                break;
            case "loadPrinter": 
                LoadBluetoothPrinter("initialize");
                break;
            case "printReceipt": 
                // input vars: trx_type, buyer_co_name, seller_co_name, amount, approval_num
                String trx_type = data.getString(0);
                String buyer_co_name = data.getString(1);
                String seller_co_name = data.getString(2);
                String amount = data.getString(3);
                String approval_num = data.getString(4);
                
                if (LoadBluetoothPrinter("pre-print")==true) { 
                    printReceipt(trx_type,
                                buyer_co_name,
                                seller_co_name,
                                amount,
                                approval_num);
                }
                break;
            case "bluetoothPrinterTest":
                if (LoadBluetoothPrinter("pre-print")==true) { 
                    bluetoothPrinterTest();
                }
                break;
            case "printBitmapTest": 
                if (LoadBluetoothPrinter("pre-print")==true) { 
                    printBitmapTest();
                }
                break;
            default: 
                callbackContext.error("\"" + action + "\" is not a recognized action.");
                return false;
        }
        return true;
    }

      public boolean LoadBluetoothPrinter(String load_type)
    {
        //Toast.makeText(cordova.getActivity(), "LoadBluetoothPrinter() ran!", Toast.LENGTH_LONG).show();
        
        // 1: Get BluetoothAdapter
        mBluetoothAdapter = BluetoothUtil.getBluetoothAdapter();
        if(mBluetoothAdapter == null)
        {
            Toast.makeText(cordova.getActivity(), "Bluetooth not enabled!", Toast.LENGTH_LONG).show();
            isBluetoothOpen = false;
            return false;
        }
        else
        {
            isBluetoothOpen =true;
        }
        //2: Get bluetoothPrinter Devices
        mBluetoothPrinterDevice = BluetoothUtil.getIposPrinterDevice(mBluetoothAdapter);
        if(mBluetoothPrinterDevice == null)
        {
            Toast.makeText(cordova.getActivity(), "Bluetooth printer failed to load.", Toast.LENGTH_LONG).show();
            return false;
        }
        //3: Get conect Socket
        try {
            socket = BluetoothUtil.getSocket(mBluetoothPrinterDevice);
        }
        catch (IOException e)
        {
            //e.printStackTrace();
            Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }

        if (load_type == "initialize") {
            Toast.makeText(cordova.getActivity(), "Bluetooth Printer Driver successfully loaded!", Toast.LENGTH_LONG).show();
        }
        return true;
    }

    private void printReceipt(String trx_type, 
                              String buyer_co_name, 
                              String seller_co_name, 
                              String amount, 
                              String approval_num)
    {
        ThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
               try{
                    Resources activityRes = cordova.getActivity().getResources();
                    int be_logo_black = activityRes.getIdentifier("be_logo_black", "drawable", cordova.getActivity().getPackageName());
                    Bitmap mBitmap1 = BitmapFactory.decodeResource(activityRes, be_logo_black);
                    
                    byte[] printer_init = ESCUtil.init_printer();
                    byte[] leftMargin0 = ESCUtil.printLeftMargin(0);

                    byte[] fontSize0 = ESCUtil.fontSizeSet((byte) 0x00);
                    byte[] h2_fontSize = ESCUtil.fontSizeSet((byte) 0x01);
                    byte[] fontSize2 = ESCUtil.fontSizeSet((byte) 0x10);
                    
                    byte[] h1_fontSize = ESCUtil.fontSizeSet((byte) 0x11); //larger font size
                    byte[] h1_lineHeight = ESCUtil.setLineHeight((byte)26); //larger line height
                   // byte[] h2_lineHeight = ESCUtil.setLineHeight((byte)50); //smaller line height

                    byte[] align_left = ESCUtil.alignMode((byte)0);
                    byte[] align_center = ESCUtil.alignMode((byte)1);
                    byte[] align_right = ESCUtil.alignMode((byte)2);
                    
                    byte[] nextLine = ESCUtil.nextLines(1);
                    byte[] performPrint = ESCUtil.performPrintAndFeedPaper((byte)200);

                    byte[][] cmdBytes = {printer_init,
                                         leftMargin0,
                                         align_center, //sets alignment to center
                                         BitMapUtil.getBitmapPrintData(mBitmap1,390,1), //be logo
                                         h1_lineHeight, //sets h1 line height
                                         //h1_fontSize, //h1 font size
                                         h2_fontSize, //sets smaller font size
                                         "Transaction Approved!\n".getBytes("GBK"), //business express\n
                                         nextLine, nextLine,
                                         "Transaction Details:\n".getBytes("GBK"), //business express\n
                                         
                                         "Type: ".getBytes(), trx_type.getBytes(), "\n".getBytes(),
                                         "Buyer: ".getBytes(), buyer_co_name.getBytes("GBK"), "\n".getBytes(),
                                         "Seller: ".getBytes(), seller_co_name.getBytes("GBK"), "\n".getBytes(),
                                         "Amount: $".getBytes("GBK"), amount.getBytes(), "\n".getBytes(),
                                         "Approval #: ".getBytes("GBK"), approval_num.getBytes(), "\n".getBytes(),
                                         performPrint};
                    
                    try {
                        if((socket == null) || (!socket.isConnected()))
                        {
                            socket = BluetoothUtil.getSocket(mBluetoothPrinterDevice);
                        }
                        byte[] data = ESCUtil.byteMerger(cmdBytes);
                        OutputStream out = socket.getOutputStream();
                        out.write(data,0,data.length);
                        out.close();
                        socket.close();
                    }
                    catch (IOException e)
                    {
                        //e.printStackTrace();
                        Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                catch (UnsupportedEncodingException e)
                {
                    //e.printStackTrace();
                    Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    return;
                }

            }
        });
    } //END printReceipt



       private void bluetoothPrinterTest()
    {
        ThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {
               try{
                    
                    byte[] printer_init = ESCUtil.init_printer();
                    byte[] fontSize0 = ESCUtil.fontSizeSet((byte) 0x00);
                    byte[] fontSize1 = ESCUtil.fontSizeSet((byte) 0x01);
                    byte[] fontSize2 = ESCUtil.fontSizeSet((byte) 0x10);
                    byte[] fontSize3 = ESCUtil.fontSizeSet((byte) 0x11);
                    byte[] lineH0 = ESCUtil.setLineHeight((byte)26);
                    byte[] lineH1 = ESCUtil.setLineHeight((byte)50);

                    byte[] align0 = ESCUtil.alignMode((byte)0);
                    byte[] align1 = ESCUtil.alignMode((byte)1);
                    byte[] align2 = ESCUtil.alignMode((byte)2);
                    byte[] title1 = "BUSINESS EXPRESS\n".getBytes("GBK");
                    byte[] title2 = "Casey\n".getBytes(); //
                    byte[] sign1 = "************************\n".getBytes("GBK");
                    byte[] fontTest0 = "This is a line of default size font\n".getBytes("GBK");
                    byte[] fontTest1 = "This is a double-height font\n".getBytes("GBK");
                    byte[] fontTest2 = "This is a line of double-width font\n".getBytes("GBK");
                    byte[] fontTest3 = "This is a double-width double-height font\n".getBytes("GBK");
                    byte[] orderSerinum = "1234567890\n".getBytes("GBK");
                    byte[] specialSign= "!@#$%^&*(κρχκμνκλρκνκνμρτυφ)\n".getBytes("GBK");
                    byte[] testSign = "------------------------\n".getBytes("GBK");
                    byte[] testInfo = "Welcome to Bluetooth printer\n".getBytes("GBK");
                    byte[] nextLine = ESCUtil.nextLines(1);
                    byte[] performPrint = ESCUtil.performPrintAndFeedPaper((byte)200);

                    byte[][] cmdBytes = {printer_init,
                                         lineH0, //set line height
                                         fontSize3, //sets font size
                                         align1, //sets alignment to center
                                         title1, //business express\n
                                         fontSize1, //sets smaller font size
                                         title2,
                                         nextLine,align0, //nextLine needed to set alignment left here
                                         fontSize0, //sets smaller font size
                                         sign1,  //line of asterisks
                                         //fontSize0, //is this needed? I commented this to find out
                                         fontTest0,
                                         lineH1,
                                         fontSize1,
                                         fontTest1,
                                         lineH0,
                                         fontSize2,
                                         fontTest2,
                            lineH1,fontSize3,fontTest3,align2,lineH0,fontSize0,orderSerinum,specialSign,testSign,
                            align1,fontSize1,lineH1,testInfo,nextLine,performPrint};
                    
                    try {
                        if((socket == null) || (!socket.isConnected()))
                        {
                            socket = BluetoothUtil.getSocket(mBluetoothPrinterDevice);
                        }
                        byte[] data = ESCUtil.byteMerger(cmdBytes);
                        OutputStream out = socket.getOutputStream();
                        out.write(data,0,data.length);
                        out.close();
                        socket.close();
                    }
                    catch (IOException e)
                    {
                        //e.printStackTrace();
                        Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                catch (UnsupportedEncodingException e)
                {
                    //e.printStackTrace();
                    Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    return;
                }

            }
        });
    }


    private void printBitmapTest()
    {
      
        ThreadPoolManager.getInstance().executeTask(new Runnable() {
            @Override
            public void run() {

                
            try {
                //Bitmap mBitmap1 = BitmapFactory.decodeResource(getResources(), R.mipmap.test_p);
                //Bitmap mBitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.be_logo_black);
                //Bitmap mBitmap1 = BitmapFactory.decodeFile("file:///android_asset/" +"www/img/be_logo_black.jpg");
                //Bitmap mBitmap1 = BitmapFactory.decodeFile("be_logo_black.jpg");
                //Bitmap mBitmap1 = BitmapFactory.decodeFile(cordova.file.applicationDirectory +"www/img/be_logo_black.jpg");
                Resources activityRes = cordova.getActivity().getResources();
                int be_logo_black = activityRes.getIdentifier("be_logo_black", "drawable", cordova.getActivity().getPackageName());
                Bitmap mBitmap1 = BitmapFactory.decodeResource(activityRes, be_logo_black);
                
                //Application app = cordova.getActivity().getApplication();
                //String package_name = app.getPackageName();
                //Resources resources = app.getResources();
                //int ic = resources.getIdentifier("be_logo_black", "drawable", package_name);
                //Bitmap mBitmap1 = BitmapFactory.decodeResource(resources, ic);

                byte[] printer_init = ESCUtil.init_printer();
                byte[] selectChinese = ESCUtil.selectChineseMode();
                byte[] charCode = ESCUtil.selectCharCodeSystem((byte) 0x01);
                byte[] align0 = ESCUtil.alignMode((byte) 0);
                byte[] align1 = ESCUtil.alignMode((byte) 1);
                byte[] align2 = ESCUtil.alignMode((byte) 2);
                byte[] leftMargin0 = ESCUtil.printLeftMargin(0);
                byte[] nexLine = ESCUtil.nextLines(1);
                byte[] performPrint = ESCUtil.performPrintAndFeedPaper((byte) 160);

                byte[][] cmdBytes = {printer_init, selectChinese, charCode,leftMargin0,
                        //align0, BitMapUtil.getBitmapPrintData(mBitmap2,128,0),
                        //align1, BitMapUtil.getBitmapPrintData(mBitmap2,128,1),
                        //align2,BitMapUtil.getBitmapPrintData(mBitmap2,128,32),
                };
                byte[][] cmdBytes1 ={leftMargin0,
                        //align0, BitMapUtil.getBitmapPrintData(mBitmap1,128,0),
                        //align1, BitMapUtil.getBitmapPrintData(mBitmap1,256,0),
                        //align2,  BitMapUtil.getBitmapPrintData(mBitmap1,320,1),
                        align1, BitMapUtil.getBitmapPrintData(mBitmap1,400,1),
                        performPrint
                };

                
                    if((socket == null) || (!socket.isConnected()))
                    {
                        socket = BluetoothUtil.getSocket(mBluetoothPrinterDevice);
                    }
                    byte[] data = ESCUtil.byteMerger(cmdBytes);
                    OutputStream out = socket.getOutputStream();
                    out.write(data, 0, data.length);
                    byte[] data1 = ESCUtil.byteMerger(cmdBytes1);
                    out.write(data1, 0, data1.length);
                    out.close();
                } catch (Exception e) {
                    //e.printStackTrace();
                    Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
        
    }

}